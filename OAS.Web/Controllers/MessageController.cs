﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.Controllers
{
    using Logic.Operations;
    using Logic.Types;

    public class MessageController : Controller
    {
        // GET: Message
        public ActionResult Index()
        {
            return View(Get.AllThreads().Where(thrds => thrds.Recipients.Any(usr => usr.Id == Authentication.AuthenticatedAs.Id)) as IEnumerable<Thread>);
            //this gets all threads where the current user has ANY relation to them as a recipient
        }

        [HttpPost]
        public ActionResult sendMessage(string threadId, string messageBody)
        {
            Get.SingleThread(int.Parse(threadId))
               .sendMessage(Authentication.AuthenticatedAs, messageBody);

            return null;
        }

        [HttpPost]
        public ActionResult createThread(string[] usernames)
        {
            var newusernames = new List<string>();

            usernames.ToList().ForEach(usernm =>
            {
                var tempName = usernm;

                tempName = tempName.Substring(tempName.IndexOf("(") + 1);

                tempName = tempName.Replace(")", "");

                newusernames.Add(tempName);
            });

            var users = Get.AllUsers().Where(x => x.IsActive && x.Id != 1);
            var filteredUsers = new List<User>();

            foreach(var user in users)
            {
                foreach(var username in newusernames)
                {
                    if (username == user.UserName)
                    {
                        filteredUsers.Add(Get.SingleUser(username));
                    }
                }
            }

            filteredUsers.Add(Authentication.AuthenticatedAs);

            Create.NewThread(filteredUsers);

            TempData["message"] = "New Thread created!";
            return RedirectToAction("Index");
        }
    }
}