﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.Controllers
{
    using static Logic.Session;
    using Logic.Types;
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error(string errorMessage, string errorType, string stackTrc, string src)
        {
            var ex = new LogEntry(errorMessage, Authentication.IsAuthenticated ? Authentication.AuthenticatedAs.Id : 2, errorType, src);

            using (var session = GetSession())
            {
                session.Save(ex);
                session.Flush();
            }

            TempData["badAlert"] = true;

            TempData["message"] = "We're very sorry, but something went wrong. An Admin will work on it.";

            return RedirectToAction("Index");
        }

        public ActionResult IndexError()
        {
            TempData["badAlert"] = true;
            TempData["message"] = "That requires Admin access!";

            var ex = new LogEntry("Unauthorized access attempt", Authentication.AuthenticatedAs.Id, "UnauthorizedAccessException", "Auth");

            using (var session = GetSession())
            {
                session.Save(ex);
                session.Flush();
            }

            return RedirectToAction("Index");
        }
    }
}