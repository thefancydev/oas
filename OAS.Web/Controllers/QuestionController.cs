﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.Controllers
{
    using Logic.Operations;
    using Logic.Types;
    using System.Text.RegularExpressions;

    public class QuestionController : Controller
    {
        // GET: Question
        public ActionResult Ask()
        {
            return View();
        }

        [HttpPost]
        public ActionResult addQuestion(string title, string body, string occupancy)
        {
            var newoccupancy = Regex.Replace(occupancy, @" \([0-9]* questions\)", "");

            if (!Get.AllOccupations().Any(x => x.Title == newoccupancy)) //if the user decided to edit the HTML before post back
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";
                return RedirectToAction("Ask");
            }
            else
            {
                Create.NewQuestion(title, body, Get.SingleOccupation(newoccupancy), Authentication.AuthenticatedAs);


                TempData["message"] = "Question created!";

                return RedirectToAction("Index", "Home");
            }
        }

        [Route("Question/{id}")]
        public ActionResult QuestionIndex(int id)
        {
            return View(Get.SingleQuestion(id));
        }

        public ActionResult secondQuestion(int questionId)
        {
            var question = Get.SingleQuestion(questionId);

            question.Second(Authentication.AuthenticatedAs);
            
            TempData["message"] = "Question seconded!";

            return RedirectToAction("QuestionIndex", new { id = question.Id });
        }

        public ActionResult unsecondQuestion(int questionId)
        {
            var question = Get.SingleQuestion(questionId);

            question.removeSecond(Authentication.AuthenticatedAs);

            
            TempData["message"] = "Question unseconded!";

            return RedirectToAction("QuestionIndex", new { id = question.Id });
        }

        public ActionResult editQuestion(int questionId)
        {
            var question = Get.SingleQuestion(questionId);

            if (question.Owner.UserName == Authentication.AuthenticatedAs.UserName || Authentication.AuthenticatedAs.IsAdmin) //? if the user decided to try and edit via URL
            {
                return View(question);
            }
            else
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";

                return RedirectToAction("QuestionIndex", new { id = questionId });
            }
        }

        public ActionResult updateQuestion(int questionId, string newtitle, string newbody, string newOccupancy)
        {
            var question = Get.SingleQuestion(questionId);

            question.Title = newtitle;
            question.Body = newbody;
            
            if (newOccupancy != question.AskedOccupation.Title)
            {
                question.AskedOccupation = Get.SingleOccupation(newOccupancy);
            }

            foreach (var ans in question.Answers)
            {
                foreach (var userOcc in ans.Owner.Occupations)
                {
                    if (userOcc.BaseOccupation.Id != question.AskedOccupation.Id)
                    {
                        Delete.deleteAnswer(ans.Id);
                    }
                }
            }
            question.update();

            
            TempData["message"] = "Successfully updated!";

            return RedirectToAction("QuestionIndex", new { id = questionId });
        }

        public ActionResult deleteQuestion(int questionId)
        {
            var question = Get.SingleQuestion(questionId);

            if (question.Owner.UserName == Authentication.AuthenticatedAs.UserName || Authentication.AuthenticatedAs.IsAdmin)
            {
                Delete.deleteQuestion(questionId);

                
                TempData["message"] = $"Question '{question.Title}' has been deleted!";

                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["badAlert"] = true;
                TempData["message"] = $"Nice try!";

                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult answerQuestion(int questionId, int ownerId, string answerBody)
        {
            if (ownerId == Authentication.AuthenticatedAs.Id && Authentication.AuthenticatedAs.Occupations.Any(x => x.BaseOccupation.Id == Get.SingleQuestion(questionId).AskedOccupation.Id))
            {
                Create.NewAnswer(ownerId, questionId, answerBody);
                Get.SingleQuestion(questionId).Owner.Notify($"New answer to {Get.SingleQuestion(questionId).Title}!", "/Question/QuestionIndex/" + questionId);
                TempData["message"] = "Question answered!";
                return RedirectToAction("QuestionIndex", new { id = questionId });
            }
            else
            {
                TempData["message"] = "Nice try!";
                return RedirectToAction("QuestionIndex", new { id = questionId });
            }
        }


        public ActionResult secondAnswer(int answerId)
        {
            var ans = Get.SingleAnswer(answerId);

            ans.Second(Authentication.AuthenticatedAs);

            TempData["message"] = "Answer seconded!";
            return RedirectToAction("QuestionIndex", new { id = ans.AnsweredQuestion.Id });

        }


        public ActionResult unsecondAnswer(int answerId)
        {
            var ans = Get.SingleAnswer(answerId);

            ans.removeSecond(Authentication.AuthenticatedAs);

            TempData["message"] = "Answer unseconded!";
            return RedirectToAction("QuestionIndex", new { id = ans.AnsweredQuestion.Id });

        }

        public ActionResult editAnswer(int answerId)
        {
            if (Get.SingleAnswer(answerId).Owner.Id == Authentication.AuthenticatedAs.Id)
                return View(Get.SingleAnswer(answerId));
            else
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";

                return RedirectToAction("QuestionIndex", new { id = Get.SingleAnswer(answerId).AnsweredQuestion.Id });
            }
        }

        public ActionResult deleteAnswer(int answerId)
        {
            var tempAnswerId = Get.SingleAnswer(answerId).AnsweredQuestion.Id; //? throws null exception due to answer having been deleted by the time I return

            Delete.deleteAnswer(answerId);

            TempData["message"] = "Answer deleted!";

            return RedirectToAction("QuestionIndex", new { id = tempAnswerId });
        }

        public ActionResult flagQuestion(int questionId, string reason)
        {
            Get.SingleQuestion(questionId).flag(Authentication.AuthenticatedAs, reason);

            TempData["message"] = "Question has been flagged! Reason: " + reason;

            return RedirectToAction("QuestionIndex", new { id = questionId });
        }

        public ActionResult unflagQuestion(int questionId)
        {
            Get.SingleQuestion(questionId).unflag(Authentication.AuthenticatedAs);

            TempData["message"] = "Question has been unflagged!";

            return RedirectToAction("QuestionIndex", new { id = questionId });
        }


    }
}