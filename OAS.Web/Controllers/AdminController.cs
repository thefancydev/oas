﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.Controllers
{
    using filters;
    using Logic.Operations;
    public class AdminController : Controller
    {
        // GET: Admin
        [AdminAccessRequired]
        public ActionResult Index()
        {
            return View();
        }

        [AdminAccessRequired]
        public ActionResult reactivate(string username)
        {
            var usr = Get.SingleUser(username);

            usr.Reactivate();

            TempData["message"] = $"User '{usr.FullName}' has been reactivated!";

            return RedirectToAction("Index");
        }

        [AdminAccessRequired]
        public ActionResult Notify(string message, string userId)
        {
            var user = Get.SingleUser(int.Parse(userId));

            user.Notify(message);

            TempData["message"] = $"Notified {user.FirstName}!";

            return RedirectToAction("Index");
        }

        [AdminAccessRequired]
        public ActionResult deleteUser(int userId)
        {
            Delete.deleteUser(userId);

            TempData["message"] = "User has been deleted!";

            return RedirectToAction("Index");
        }

        [AdminAccessRequired]
        public ActionResult createUser(string Username, string Firstname, string Lastname, string Email, string Password, string admin = null)
        {
            if (Get.SingleUser(Username) != null)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Username is already taken!";

                return RedirectToAction("Index");
            }

            Create.NewUser(Username, Password, Firstname, Lastname, Email, admin == "on" ? true : false);

            TempData["message"] = $"User {Username} is created!";

            return RedirectToAction("Index");
        }

        public ActionResult Impersonate(int Id)
        {
            if (!Authentication.AuthenticatedAs.IsAdmin)
            {
                return RedirectToAction("IndexError", "Home");
            }
            else
            {
                Authentication.ImpersonatedFrom = Authentication.AuthenticatedAs;

                Authentication.AuthenticatedAs = Get.SingleUser(Id);

                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult unimpersonate()
        {
            if (!Authentication.ImpersonatedFrom.IsAdmin)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";

                return RedirectToAction("Index", "Home");
            }
            Authentication.AuthenticatedAs = Authentication.ImpersonatedFrom;
            Authentication.ImpersonatedFrom = null;

            TempData["message"] = "Impersonation has ended. Welcome back " + Authentication.AuthenticatedAs.FirstName + "!";

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult editUser(int userId, string Username, string Firstname, string Lastname, string Email, string Password, string Admin)
        {
            Update.updateUser(userId, new Logic.Types.User(Username, Password, Firstname, Lastname, Email, Admin == "on" ? true : false));

            TempData["message"] = "User has been updated!";

            return RedirectToAction("Index");
        }

        
    }
}