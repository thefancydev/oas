﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Extensions;

namespace OAS.Web.Controllers
{
    using Logic.Operations;
    using filters;
    using Logic.Types;
    using System.Text;

    public class AccountController : Controller
    {
        [SkipAuthentication]
        public ActionResult resetPasswordPage()
        {
            return View(); //the enter email page
        }

        [HttpPost] [SkipAuthentication]
        public ActionResult sendResetEmail(string username)
        {
            var user = Get.SingleUser(username);

            if (user != null)
            {
                user.email("Hi There, <br />you recenty requested a password reset. Click on the below link to do so, or just ignore this email if this wasn't you", "OAS Password Reset", Url.Action("resetPassword", "Account", new { token = Security.registerResetToken(user) }, Request.Url.Scheme ));

                TempData["message"] = "Check your inbox!";

                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "This user doesn't exist";

                return RedirectToAction("Index");
            }
            
        }

        [SkipAuthentication]
        public ActionResult resetPassword(string token)
        {
            if (Security.validateResetToken(token))
            {
                TempData["token"] = token;
                return View();
            }
            else
            {
                TempData["message"] = "Token is incorrect!";
                return RedirectToAction("LoginRegister");
            }
        }

        [SkipAuthentication] [HttpPost]
        public ActionResult createNewPassword(string token, string newPassword)
        {
            if (Security.validateResetToken(token))
            {
                Security.useResetToken(token, newPassword);

                TempData["message"] = "Password has been reset!";

                return RedirectToAction("LoginRegister");
            }
            else
            {
                TempData["message"] = "This token is invalid!";
                return RedirectToAction("LoginRegister");
            }

            //change password here
        }

        public ActionResult AddOccupationType(string title, string description)
        {
            if (Get.SingleOccupation(title) == null)
            {
                //requested occupation doesn't exist

                Create.NewOccupationType(title, description, Authentication.AuthenticatedAs.Id);
                TempData["message"] = $"Occupation type '{title}' has been created!";
            }
            else
            {
                TempData["badAlert"] = true;
                TempData["message"] = $"Occupation type '{title}' already exists";
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult markNotificationRead(string notificationId)
        {
            Authentication.AuthenticatedAs.NotificationsRecieved.Single(x => x.Id == int.Parse(notificationId)).MarkRead();

            return null;
        }

        [HttpGet]
        public string GetUnreadNotification()
        {
            Authentication.reload();
            var sb = new StringBuilder();

            foreach (var notification in Authentication.AuthenticatedAs.NotificationsRecieved.Where(x => !x.Read).ToArray())
            {
                if (notification.Id == Authentication.AuthenticatedAs.NotificationsRecieved.Last().Id)
                {
                    sb.Append($"{notification.Id}~;{notification.Body.Truncate(20)}~;{notification.DateSent.ToFuzzyDate()}");
                }
                else
                {
                    sb.Append($"{notification.Id}~;{notification.Body.Truncate(20)}~;{notification.DateSent.ToFuzzyDate()};;");
                }
            }

            return sb.ToString();


            
        }
        public ActionResult Index(string username = null)
        {
            if (!Authentication.IsAuthenticated)
                return RedirectToAction("LoginRegister"); //? FIX: page throws null error due to code executing on AuthenticatedAs before the filter kicks in
            User checkUser;
            if (username == Authentication.AuthenticatedAs.UserName || username == null)
                checkUser = Authentication.AuthenticatedAs;
            else
                checkUser = Get.SingleUser(username);

            if (!checkUser.IsActive)
            {
            	TempData["badAlert"] = true;
            	TempData["message"] = "This user is no longer active";

            	return RedirectToAction("Index", "Home");
            }
            else
            {
            	return View(checkUser);
            }
        }

        [HttpPost]
        public ActionResult addOccupation(string occupationType, string company, string startDate, string endDate = null)
        {
            if (String.IsNullOrEmpty(endDate)) //? if currentJob
            {
                if (Authentication.AuthenticatedAs.CurrentOccupation != null)
                    Update.UpdateOccupancy(Authentication.AuthenticatedAs.CurrentOccupation.Id, new Occupation(Authentication.AuthenticatedAs.CurrentOccupation.Company, Authentication.AuthenticatedAs.CurrentOccupation.BaseOccupation, Authentication.AuthenticatedAs.CurrentOccupation.StartDate, DateTime.Now, Authentication.AuthenticatedAs));
                //this wordy statement makes users avoid multiple current jobs (may need changing about for part time jobs)

                Create.NewOccupation(Authentication.AuthenticatedAs.Id, company, DateTime.Parse(startDate), null, Get.SingleOccupation(occupationType));
            }
            else
                Create.NewOccupation(Authentication.AuthenticatedAs.Id, company, DateTime.Parse(startDate), DateTime.Parse(endDate), Get.SingleOccupation(occupationType));


            TempData["message"] = "Successfully registered occupation";

            return RedirectToAction("Index", "Home");
        }

        [SkipAuthentication]
        public ActionResult LoginRegister()
        {
            if (!Authentication.IsAuthenticated)
                return View();
            else
                return RedirectToAction("Index"); //? stops the user breaking authentication
        }

        [SkipAuthentication]
        [HttpPost]
        public ActionResult RegisterAccount(string firstname, string lastname, string password, string username, string email)
        {
            if (Get.SingleUser(username) != null)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "That username is taken!";

                return RedirectToAction("LoginRegister");
            }
            else
            {
                Create.NewUser(username, password, firstname, lastname, email);

                Authentication.AuthenticatedAs = Get.SingleUser(username); //? can't assign directly due to Id being generated by database (this logs in on redirect)


                TempData["message"] = $"New account created, welcome {Authentication.AuthenticatedAs.FullName}!";

                return RedirectToAction("Index", "Home");
            }
        }

        [SkipAuthentication]
        [HttpGet]
        public ActionResult CheckLogin(string username, string password)
        {
            var wantedUser = Get.SingleUser(username);

            if (wantedUser == null)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "This user doesn't exist!";
                return RedirectToAction("LoginRegister");
            }
            else
            {
                if (!Security.ValidatePassword(password, wantedUser.PasswordHash))
                {
                    TempData["badAlert"] = true;
                    TempData["message"] = "Password is incorrect!";
                    return RedirectToAction("LoginRegister");
                }
                else
                {
                    if (wantedUser.IsActive)
                    {

                        TempData["message"] = "Logged in successfully!";
                        Authentication.AuthenticatedAs = Get.SingleUser(username);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData["badAlert"] = true;
                        TempData["message"] = "Account isn't active! Please contact the admin";

                        return RedirectToAction("LoginRegister");
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult editOccupation(int occupationId, string newStartDate, string newCompany, string newOccupationType, string newEndDate = null)
        {
            // Dear Developer:
            // 
            // Once you are done trying to 'optimize' this method,
            // and have realized what a terrible mistake that was,
            // please increment the following counter as a warning
            // to the next guy:
            // 
            // total_hours_wasted_here = 10

            //It's confusing, so I'll (try to) explain

            if (!Authentication.AuthenticatedAs.Occupations.Any(x => x.Id == occupationId)) //? if not of the users occupations have the provided ID (if this isn't his/her occupation)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";

                return RedirectToAction("Index");
            }
            else
            {
                var occ = Authentication.AuthenticatedAs.Occupations.Single(x => x.Id == occupationId); //? gets the occupation from the authenticated ID.

                occ.StartDate = DateTime.Parse(newStartDate);

                if (String.IsNullOrEmpty(newEndDate))
                    occ.EndDate = null;
                else
                    occ.EndDate = DateTime.Parse(newEndDate);

                occ.Company = newCompany;

                if (occ.BaseOccupation.Title != newOccupationType && newOccupationType != null) //? if the user changed this answers shouldn't be left (&& is due to the option not being selected returns null)
                {
                    List<Answer> allowedAnswers = new List<Answer>();
                    foreach (var answer in Authentication.AuthenticatedAs.Answers)
                    {
                        foreach (var occupation in Authentication.AuthenticatedAs.Occupations)
                        {
                            if (answer.AnsweredQuestion.AskedOccupation.Id == occupation.Id)
                            {
                                allowedAnswers.Add(answer);
                                break;
                            }
                        }
                    }

                    var unallowedAnswers = new List<Answer>();

                    foreach (var answer in allowedAnswers)
                    {
                        unallowedAnswers.Add(Authentication.AuthenticatedAs.Answers.Where(x => x.Id != answer.Id).Single());
                    }

                    unallowedAnswers.ForEach(x => Delete.deleteAnswer(x.Id));
                }

                Update.UpdateOccupancy(occupationId, new Occupation(occ.Company, occ.BaseOccupation, occ.StartDate, occ.EndDate, Authentication.AuthenticatedAs));

                Authentication.reload();


                TempData["message"] = "Occupation has been edited!";

                return RedirectToAction("Index");
            }
        }

        public ActionResult logout()
        {
            Authentication.AuthenticatedAs = null;


            TempData["message"] = "Logged out successfully!";

            return RedirectToAction("Index");
        }

        public ActionResult deleteOccupation(int occupationId)
        {
            try
            {
                Delete.deleteOccupation(Authentication.AuthenticatedAs.Id, occupationId); //? will throw an error if the occupation doesn't exist under current user
                Authentication.reload();

            }
            catch
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Nice try!";

                return RedirectToAction("Index", "Home");
            }



            TempData["message"] = "Occupation has been deleted!";

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult editAccount(string firstname, string lastname, string email, string oldpassword, string newpassword, string confirmnewpassword) //? doesn't need user Id, just go off logged in user
        {
            if (!Security.ValidatePassword(oldpassword, Authentication.AuthenticatedAs.PasswordHash) && (newpassword != "" && confirmnewpassword != ""))
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Old password is incorrect!";

                return RedirectToAction("Index");
            }

            if (newpassword != confirmnewpassword)
            {
                TempData["badAlert"] = true;
                TempData["message"] = "Passwords don't match!";

                return RedirectToAction("Index");
            }

            Update.updateUser(Authentication.AuthenticatedAs.Id, new User(Authentication.AuthenticatedAs.UserName, newpassword == "" ? "placeholder" : newpassword, firstname, lastname, email, Authentication.AuthenticatedAs.IsAdmin));

            TempData["message"] = "Account updated!";

            return RedirectToAction("Index");
        }
    }
}