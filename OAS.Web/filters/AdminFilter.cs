﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.filters
{
    public class AdminAccessRequired : Attribute
    {
        //! Hacky way of requiring admin access
    }
    public class AdminFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(AdminAccessRequired), false).Any())
            {
                if (!Authentication.AuthenticatedAs.IsAdmin)
                {
                    var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                    
                    filterContext.Result = new RedirectResult(urlHelper.Action("IndexError", "Home"));
                }
            }
            else
            {
                
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }
    }
}