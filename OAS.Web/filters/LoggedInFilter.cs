﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAS.Web.filters
{
    public class SkipAuthentication : Attribute
    {
        //! Hacky way of stopping redirect loop with Login/Register page
    }

    public class LoggedInFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipAuthentication), false).Any())
            {
                
            }
            else
            {
                if (!Authentication.IsAuthenticated)
                {
                    var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                    filterContext.Result = new RedirectResult(urlHelper.Action("LoginRegister", "Account"));
                }
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }
    }
}