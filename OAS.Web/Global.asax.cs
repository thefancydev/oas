﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OAS.Web
{
    using static Logic.Session;
    using Logic.Types;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Error(object sender, EventArgs e)
        {
            var exc = Server.GetLastError();

            Server.ClearError();

            Response.Clear();
#if DEBUG
            var responseURL = $"/Home/Error?errorMessage={exc.Message}&errorType={exc.GetType().Name}&src={exc.Source}".Replace(" ", "%20").Replace("\r", "").Replace("\n", "");
            Response.Redirect(responseURL);
#else
            Response.Redirect("/oas/Home/Error?errorMessage=" + exc.Message + "&errorType=" + exc.GetType().Name);
#endif
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
