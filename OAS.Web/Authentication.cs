﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAS.Web
{
    using Logic.Types;
    using Logic.Operations;
    public static class Authentication
    {
        public static User AuthenticatedAs { get; set; }
        public static bool IsAuthenticated => AuthenticatedAs == null ? false : true;

        public static User ImpersonatedFrom { get; set; }
        public static bool isImpersonated => ImpersonatedFrom == null ? false : true;


        public static void reload()
        {
            AuthenticatedAs = Get.SingleUser(AuthenticatedAs.Id);
        }
    }
}