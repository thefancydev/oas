@echo off

set /p deploy="Do you wish to deploy to FTP as well? (Y/N): "

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsMSBuildCmd.bat"

set _my_datetime=%date%_%time%
set _my_datetime=%_my_datetime: =_%
set _my_datetime=%_my_datetime::=.%
set _my_datetime=%_my_datetime:/=_%
set _my_datetime=%_my_datetime:.=_%

REM removes illegal characters

SET fileName=%_my_datetime%-%username%.log
@echo on

nuget restore
if /I "%deploy%" EQU "Y" (MSBuild /verbosity:detailed /m /fl /flp:logfile=BuildLog/%fileName%;verbosity=diagnostic /p:DeployOnBuild=true;PublishProfile=FTP) ELSE (MSBuild /verbosity:detailed /m /fl /flp:logfile=BuildLog/%fileName%;verbosity=diagnostic)

pause