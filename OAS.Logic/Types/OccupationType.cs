﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class OccupationType
    {
        public OccupationType()
        {
            
        }

        public OccupationType(string title, string description, int creatorid)
        {
            Description = description;
            Title = title;
            Creator = Operations.Get.SingleUser(creatorid);
        }
        public virtual int Id { get; protected set; }

        public virtual string Title { get; set; }
        public virtual string Description { get; set; }

        public virtual User Creator { get; set; }

        public virtual IList<Question> AskedQuestionsForOccupation => Operations.Get.AllQuestions().Where(x => x.AskedOccupation.Id == this.Id).ToList();
    }
}
