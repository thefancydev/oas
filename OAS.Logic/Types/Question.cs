﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    using static Session;
    public class Question
    {
        public Question()
        {
            SecondedUsers = new List<User>();

            Answers = new List<Answer>();
            Comments = new List<Comment>();
            Flags = new List<Flag>();
        }

        public Question(string title, string body, User owner, OccupationType askedoccupation)
        {
            Title = title;
            Body = body;
            Owner = owner;
            AskedOccupation = askedoccupation;

            SecondedUsers = new List<User>();

            Answers = new List<Answer>();
            Comments = new List<Comment>();
            Flags = new List<Flag>();
        }

        public Question(string title, string body, User owner, Occupation askedoccupation)
        {
            Title = title;
            Body = body;
            Owner = owner;
            AskedOccupation = askedoccupation.BaseOccupation;

            SecondedUsers = new List<User>();

            Answers = new List<Answer>();
            Comments = new List<Comment>();
            Flags = new List<Flag>();
        }

        public virtual int Id { get; protected set; }

        public virtual string Title { get; set; }
        public virtual string Body { get; set; }

        public virtual User Owner { get; protected set; } //no need to be able to change question owner
        public virtual IList<User> SecondedUsers { get; set; } //almost 'votes'. no downvoting though. Ain't nobody got time for dat!

        public virtual IList<Answer> Answers { get; set; } //the 'accepted answer' field is stored in here
        public virtual IList<Comment> Comments { get; set; }

        public virtual OccupationType AskedOccupation { get; set; } //might need this :)
        public virtual IList<Flag> Flags { get; protected set; }

        public virtual void Second(User seconded)
        {
            this.SecondedUsers.Add(seconded);

            using (var session = GetSession())
            {
                session.Update(this);
                session.Flush();
            }
        }

        public virtual void removeSecond(User unsecond)
        {
            this.SecondedUsers = this.SecondedUsers.Where(x => x.UserName != unsecond.UserName) as IList<User>;

            this.update();
        }

        public virtual void flag(User flagger, string reason)
        {
            using (var session = GetSession())
            {
                session.Save(new Flag(flagger.Id, this.Id, reason));
                session.Flush();
            }
            this.update();
        }

        public virtual void unflag(User unflagger)
        {
            this.Flags = this.Flags.Where(x => x.Owner.UserName != unflagger.UserName) as IList<Flag>;

            this.update();
        }

        public virtual void update()
        {
            using (var session = GetSession())
            {
                session.Update(this);
                session.Flush();
            }
        }
    }
}
