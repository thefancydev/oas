﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class passwordToken
    {
        public passwordToken()
        {

        }

        public passwordToken(User owner)
        {
            Value = Guid.NewGuid().ToString();

            ExpiryDate = DateTime.Now.AddHours(24);

            Owner = owner;
        }

        public virtual int Id { get; protected set; }

        public virtual string Value { get; protected set; }

        public virtual User Owner { get; protected set; }

        public virtual DateTime ExpiryDate { get; protected set; }

        public virtual TimeSpan TimeLeft => ExpiryDate - DateTime.Now;
    }
}
