﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    using Operations;
    public class Flag
    {
        public Flag()
        {

        }

        public Flag(int ownerid, int questionid, string reason)
        {
            Owner = Get.SingleUser(ownerid);
            QuestionFlagged = Get.SingleQuestion(questionid);
            Reason = reason;
        }

        public Flag(int ownerid, string reason, int answerid)
        {
            Owner = Get.SingleUser(ownerid);
            AnswerFlagged = Get.SingleAnswer(answerid);
            Reason = reason;
        }

        public virtual int Id { get; protected set; }

        public virtual User Owner { get; protected set; }
        public virtual Question QuestionFlagged { get; protected set; }
        public virtual Answer AnswerFlagged { get; protected set; }

        public virtual string Reason { get; set; }

    }
}
