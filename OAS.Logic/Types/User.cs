﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Net.Mail;

namespace OAS.Logic.Types
{
    using Operations;
    using System.Drawing;
    using System.Net;
    using System.Net.NetworkInformation;
    using static Session;

    public class User
    {
        #region constructors
        public User()
        {
            Occupations = new List<Occupation>();
            Comments = new List<Comment>();

            SecondedAnswers =
                Answers =
                new List<Answer>();

            SecondedQuestions =
                Questions =
                new List<Question>();

            CreatedOccupations = new List<OccupationType>();

            NotificationsRecieved = new List<Notification>();

            SubscribedThreads = new List<Thread>();
        }

        public User(string username, string password, string firstname, string lastname, string email)
        {
            Occupations = new List<Occupation>();

            Comments = new List<Comment>();

            SecondedAnswers =
                Answers =
                new List<Answer>();

            SecondedQuestions =
                Questions =
                new List<Question>();

            UserName = username;
            PasswordHash = Security.HashPassword(password);

            FirstName = firstname;
            LastName = lastname;
            Email = email;

            CreatedOccupations = new List<OccupationType>();

            IsActive = true;

            NotificationsRecieved = new List<Notification>();

            SubscribedThreads = new List<Thread>();
        }

        public User(string username, string password, string firstname, string lastname, string email, bool admin)
        {
            Occupations = new List<Occupation>();

            Comments = new List<Comment>();

            SecondedAnswers =
                Answers =
                new List<Answer>();

            SecondedQuestions =
                Questions =
                new List<Question>();

            UserName = username;
            PasswordHash = Security.HashPassword(password);

            FirstName = firstname;
            LastName = lastname;
            Email = email;

            CreatedOccupations = new List<OccupationType>();

            IsAdmin = admin;

            IsActive = true;

            NotificationsRecieved = new List<Notification>();

            SubscribedThreads = new List<Thread>();
        }
        #endregion

        #region properties
        public virtual int Id { get; protected set; }
        public virtual string UserName { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual bool IsAdmin { get; set; }

        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FullName => FirstName + " " + LastName;
        public virtual string Email { get; set; }

        public virtual IList<Occupation> Occupations { get; set; }
        public virtual Occupation CurrentOccupation => Occupations.Where(x => x.CurrentJob == true).SingleOrDefault();

        public virtual IList<Flag> OwnedFlags { get; set; }

        public virtual IList<Answer> Answers { get; set; }
        public virtual IList<Question> Questions { get; set; }
        public virtual IList<Comment> Comments { get; set; }
        public virtual IList<Answer> SecondedAnswers { get; set; }
        public virtual IList<Question> SecondedQuestions { get; set; }

        public virtual IList<OccupationType> CreatedOccupations { get; set; }

        public virtual IList<Notification> NotificationsRecieved { get; set; }

        public virtual IList<Thread> SubscribedThreads { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int Votes
        {
            get
            {
                int totalVotes = 0;
                foreach (var ans in this.Answers) //maybe implement proper votes at some point...
                {
                    totalVotes += ans.SecondedUsers.Count;
                }

                foreach (var que in this.Questions)
                {
                    totalVotes += que.SecondedUsers.Count;
                }

                return totalVotes;
            }
        }

        public virtual string GravatarURL
        {
            get
            {
                using (MD5 md = MD5.Create())
                {
                    byte[] data = md.ComputeHash(Encoding.UTF8.GetBytes(Email));

                    StringBuilder sBuilder = new StringBuilder();

                    for (int i = 0; i < data.Length; i++)
                    {
                        sBuilder.Append(data[i].ToString("x2"));
                    }

                    try
                    {
                        using (var client = new WebClient())
                        {
                            using (var stream = client.OpenRead("http://www.google.com"))
                            {
                                return "https://www.gravatar.com/avatar/" + sBuilder.ToString();
                            }
                        }
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
        }
        #endregion

        public virtual void Deactivate()
        {
            this.IsActive = false;
            using (var session = GetSession())
            {
                session.Update(this);
            }
        }

        public virtual void Reactivate()
        {
            this.IsActive = true;
            using (var session = GetSession())
            {
                session.Update(this);
            }
        }

        public virtual void Notify(string message, string link = "#")
        {
            using (var session = GetSession())
            {
                try
                {
                    MailMessage msg = new MailMessage("roconnoremailservices@gmail.com", this.Email);
                    msg.Subject = "OAS notification recieved";
                    msg.IsBodyHtml = true;

                    msg.Body = "<h2>New notification from OAS!</h2><br /><p>You'ce recieved a new notification from OAS, click <a href=\"http://roconnor.somee.com/oas\">here</a> to view it</p>";

                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new NetworkCredential("roconnoremailservices@gmail.com", "3m@1l$ervices");  //do not bother trying to log in, as I will change the password, and if you try to again, I will chabge it again, and again, and again. You get the picture :-)
                    client.Send(msg);
                }
                catch
                {
                    session.Save(new LogEntry($"User notification for {this.UserName} failed", 1, "Email Failure", "OAS.Logic.Types.User"));
                }
                finally
                {
                    session.Save(new Notification(message, this.Id, link));
                    session.Flush();
                }
            }
        }

        public virtual void email(string content, string subject = "New notification from OAS", string link = "http://roconnor.somee.com/oas")
        {
            try
            {
                MailMessage msg = new MailMessage("roconnoremailservices@gmail.com", this.Email);
                msg.Subject = subject;
                msg.IsBodyHtml = true;

                msg.Body = content += $"<br /><br />click <a href='{link}'>here</a> to open this on OAS";

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential("roconnoremailservices@gmail.com", "3m@1l$ervices");  //do not bother trying to log in, as I will change the password, and if you try to again, I will change it again, and again, and again. You get the picture :-)
                client.Send(msg);
            }
            catch
            {
                using (var session = GetSession())
                {
                    session.Save(new LogEntry($"User notification for {this.UserName} failed", 1, "Email Failure", "OAS.Logic.Types.User"));
                }
            }
        }
    }
}
