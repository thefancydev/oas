﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    using Operations;
    using static Session;

    public class Notification
    {
        public Notification()
        {

        }

        public Notification(string message, int RecipientId, string link)
        {
            Notified = Get.SingleUser(RecipientId);
            Body = message;
            DateSent = DateTime.Now;
            Link = link;
        }

        public virtual int Id { get; protected set; }

        public virtual User Notified { get; protected set; }

        public virtual string Body { get; protected set; }

        //public virtual string Link { get; protected set; } //unlinked stuff (such as 'password changed', or 'question deleted') will just leave this null
        //UPDATE: cba to implement this

        public virtual bool Read { get; set; } // mark as unread

        public virtual string Link { get; protected set; }

        public virtual DateTime DateSent { get; protected set; }

        public virtual void MarkRead()
        {
            this.Read = true;

            using (var session = GetSession())
            {
                session.Update(this);
                session.Flush();
            }
        }
    }
}
