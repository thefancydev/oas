﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class Occupation //this is NOT A LOOKUP! This allows Occupation to have start dates and end dates
    {
        public Occupation()
        {

        }

        public Occupation(string company, OccupationType occupationtype, DateTime startdate, DateTime? enddate, User owner)
        {
            Company = company;
            BaseOccupation = occupationtype;
            StartDate = startdate;
            EndDate = enddate;
            Owner = owner;
        }

        public virtual int Id { get; protected set; }
        public virtual string Company { get; set; }

        public virtual OccupationType BaseOccupation { get; set; }
        public virtual bool CurrentJob => EndDate == null ? true : false;
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; } //instead of setting to datetime.now, just make nullable and don't set when inserting

        public virtual User Owner { get; set; }
    }
}
