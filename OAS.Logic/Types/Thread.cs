﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class Thread
    {
        public Thread()
        {
            Recipients = new List<User>();

            Messages = new List<Message>();
        }

        public Thread(IEnumerable<User> recipients)
        {
            Recipients = recipients.ToList();
        }
        public virtual int Id { get; protected set; }

        public virtual IList<User> Recipients { get; set; }

        public virtual IList<Message> Messages { get; set; }

        public virtual void sendMessage(User sender, string body)
        {
            using (var session = Session.GetSession())
            {
                session.Save(new Message(sender, this, body));

                session.Flush();
            }

            foreach (var recipient in this.Recipients)
            {
                recipient.Notify($"New message from {sender.FullName}", "/Message/Index#pane" + this.Id);
            }
        }
    }
}
