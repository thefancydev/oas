﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class Message
    {
        public Message()
        {

        }

        public Message(User sender, Thread parent, string body)
        {
            Sender = sender;
            ParentThread = parent;
            Body = body;

            SentDate = DateTime.Now;
        }
        public virtual int Id { get; protected set; }

        public virtual User Sender { get; protected set; }

        public virtual Thread ParentThread { get; protected set; }

        public virtual string Body { get; protected set; }

        public virtual DateTime SentDate { get; protected set; }
    }
}
