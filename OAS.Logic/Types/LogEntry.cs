﻿using OAS.Logic.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class LogEntry
    {
        public LogEntry()
        {
            OccurenceDate = DateTime.Now;
        }

        public LogEntry(string message, int userId, string type, string src)
        {
            ExceptionMessage = message;

            LoggedIn = Get.SingleUser(userId);

            ExceptionType = type;

            OccurenceDate = DateTime.Now;

            Source = src;
        }

        public virtual int Id { get; protected set; }

        public virtual string ExceptionMessage { get; protected set; }

        public virtual DateTime OccurenceDate { get; protected set; }

        public virtual User LoggedIn { get; protected set; }

        public virtual string ExceptionType { get; protected set; }

        public virtual string Source { get; protected set; }
    }
}
