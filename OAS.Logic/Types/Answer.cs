﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    using Operations;
    using static Session;

    public class Answer
    {
        public Answer()
        {
            Comments = new List<Comment>();
            Flags = new List<Flag>();
            SecondedUsers = new List<User>();
        }

        public Answer(int userid, int questionid, string body)
        {
            Comments = new List<Comment>();
            SecondedUsers = new List<User>();
            Flags = new List<Flag>();

            Body = body;

            AnsweredQuestion = Get.SingleQuestion(questionid);

            Owner = Get.SingleUser(userid);
        }
        public virtual int Id { get; protected set; }

        public virtual string Body { get; set; }

        public virtual User Owner { get; set; }
        public virtual IList<User> SecondedUsers { get; set; }

        public virtual bool Accepted { get; set; } //only acceptable by the question owner

        public virtual Question AnsweredQuestion { get; protected set; }

        public virtual IList<Comment> Comments { get; set; }

        public virtual IList<Flag> Flags { get; set; }

        public virtual void Second(User seconded)
        {
            this.SecondedUsers.Add(seconded);

            using (var session = GetSession())
            {
                session.Update(this);
                session.Flush();
            }
        }

        public virtual void removeSecond(User unsecond)
        {
            this.SecondedUsers = this.SecondedUsers.Where(x => x.UserName != unsecond.UserName) as IList<User>;

            using (var session = GetSession())
            {
                session.Update(this);
                session.Flush();
            }
        }
    }
}
