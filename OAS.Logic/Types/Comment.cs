﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Types
{
    public class Comment
    {
        public virtual int Id { get; protected set; }

        public virtual string Body { get; set; }

        public virtual User Owner { get; protected set; }

        public virtual Question CommentedQuestion { get; protected set; }
        public virtual Answer CommentedAnswer { get; protected set; }
    }
}
