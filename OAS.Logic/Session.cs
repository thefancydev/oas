﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic
{
    using Objects;

    public static class Session
    {
#pragma warning disable CSE0003 // Use expression-bodied members due to preprocessor directives
        private static ISessionFactory SessionFactory()
#pragma warning restore CSE0003 // Use expression-bodied members due to preprocessor directives
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql7
                                                       .ConnectionString(s =>
                                                       {
#if DEBUG
                                                           s.Server(".\\SQLEXPRESS");
                                                           s.Database("oas.DB");
                                                           s.TrustedConnection();
#else
                                                           s.Server("roconnorlivedb.mssql.somee.com");
                                                           s.Database("roconnorlivedb");
                                                           s.Username("oasSQLLogin1");
                                                           s.Password("0@$L1veDB");
#endif
                                                       }))
                           .Mappings(m => m.FluentMappings
                                           .AddFromAssemblyOf<AnswerMap>()
                                           .AddFromAssemblyOf<CommentMap>()
                                           .AddFromAssemblyOf<FlagMap>()
                                           .AddFromAssemblyOf<LogEntryMap>()
                                           .AddFromAssemblyOf<NotificationMap>()
                                           .AddFromAssemblyOf<OccupationMap>()
                                           .AddFromAssemblyOf<OccupationTypeMap>()
                                           .AddFromAssemblyOf<QuestionMap>()
                                           .AddFromAssemblyOf<UserMap>()
                                           .AddFromAssemblyOf<MessageMap>())
                           .BuildConfiguration()
                           .BuildSessionFactory();
        }

        public static ISession GetSession() => SessionFactory().OpenSession();

        public static IStatelessSession GetStatelessSession() => SessionFactory().OpenStatelessSession();
    }
}
