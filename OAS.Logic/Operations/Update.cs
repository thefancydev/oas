﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Operations
{
    using Types;
    using static Session;
    public static class Update
    {
        public static void UpdateOccupancy(int oldOccupancyId, Occupation newOccupancy)
        {
            using (var session = GetSession())
            {
                var occ = session.QueryOver<Occupation>()
                                 .Where(x => x.Id == oldOccupancyId)
                                 .SingleOrDefault();

                occ.StartDate = newOccupancy.StartDate; //? can't use a straight up assignment as it will cause Fluent to think it's a new entity
                occ.EndDate = newOccupancy.EndDate;
                occ.BaseOccupation = newOccupancy.BaseOccupation;
                occ.Company = newOccupancy.Company;

                session.Update(occ);
                session.Flush();
            }
        }

        public static void updateUser(int oldUserId, User newUser)
        {
            using (var session = GetSession())
            {
                var user = session.QueryOver<User>()
                                  .Where(x => x.Id == oldUserId)
                                  .SingleOrDefault();

                user.UserName = newUser.UserName;

                user.FirstName = newUser.FirstName;

                user.LastName = newUser.LastName;

                user.Email = newUser.Email;

                user.PasswordHash = Security.ValidatePassword("placeholder", newUser.PasswordHash) ? user.PasswordHash : newUser.PasswordHash;

                user.IsAdmin = newUser.IsAdmin;

                user.IsActive = newUser.IsActive;

                session.Update(user);

                session.Flush();
            }
        }
    }
}
