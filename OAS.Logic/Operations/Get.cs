﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Operations
{
    using Types;
    using static Session;
    public static class Get
    {
        public static OccupationType[] AllOccupations()
        {
            using (var session = GetSession())
            {
                var results =  session
                                    .QueryOver<OccupationType>()
                                    .List()
                                    .ToList();

                return results.OrderBy(x => x.Title).ToArray();
            }
        }

        public static Thread[] AllThreads()
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Thread>()
                              .List()
                              .ToArray();
            }
        }

        public static Thread SingleThread(int threadId)
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Thread>()
                              .Where(x => x.Id == threadId)
                              .SingleOrDefault();
            }
        }

        public static OccupationType SingleOccupation(string name)
        {
            try
            {
                return AllOccupations().Where(x => x.Title == name).Single();
            }
            catch
            {
                return null;
            }
        }

        public static LogEntry[] AllErrors()
        {
            using (var session = GetSession())
            {
                return session.QueryOver<LogEntry>()
                              .List()
                              .ToArray();
            }
        }

        public static LogEntry SingleError(int Id)
        {
            using (var session = GetSession())
            {
                return session.QueryOver<LogEntry>()
                              .Where(x => x.Id == Id)
                              .SingleOrDefault();
            }
        }

        public static Flag[] AllFlags()
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Flag>()
                              .List()
                              .ToArray();
            }
        }

        public static User[] AllUsers()
        {
            using (var session = GetSession())
            {
                return session
                            .QueryOver<User>()
                            .List()
                            .ToArray();
            }
        }

        public static User SingleUser(int Id)
        {
            using (var session = GetSession())
            {
                return session
                            .QueryOver<User>()
                            .Where(x => x.Id == Id)
                            .SingleOrDefault();
            }
        }

        public static User SingleUser(string username)
        {
            using (var session = GetSession())
            {
                var foo = session
                            .QueryOver<User>()
                            .Where(x => x.UserName == username)
                            .List();

                return foo.SingleOrDefault();
            }
        }

        public static Question[] AllQuestions()
        {
            using (var session = GetSession())
            {
                return session
                            .QueryOver<Question>()
                            .List()
                            .ToArray();
            }
        }

        public static Question SingleQuestion(int Id)
        {
            using (var session = GetSession())
            {
                return session
                            .QueryOver<Question>()
                            .Where(x => x.Id == Id)
                            .SingleOrDefault();
            }
        }

        public static Answer SingleAnswer(int Id)
        {
            using (var session = GetSession())
            {
                return session
                            .QueryOver<Answer>()
                            .Where(x => x.Id == Id)
                            .SingleOrDefault();
            }
        }
    }
}
