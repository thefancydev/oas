﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Operations
{
    using Types;
    using static Session;
    public static class Delete
    {
        public static void deleteUser(int userId)
        {
            using (var session = GetSession())
            {
                var user = session.QueryOver<User>()
                                  .Where(x => x.Id == userId)
                                  .SingleOrDefault();

                user.IsActive = false; //due to cascading issues

                session.Update(user);

                session.Flush();
            }
        }
        public static void deleteQuestion(int questionId)
        {
            using (var session = GetSession())
            {
                var question = session.QueryOver<Question>()
                                      .Where(x => x.Id == questionId)
                                      .SingleOrDefault();

                session.Delete(question);

                session.Flush();
            }
        }

        public static void deleteOccupation(int userId, int occupationId)
        {
            var occ = Get.SingleUser(userId).Occupations.Where(x => x.Id == occupationId).Single();

            //user.Occupations = user.Occupations.Where(x => x.Id != occupationId).ToList(); throws null insert exception as tries to leave record in DB

            if (occ.Owner.Id == userId)
            {
                using (var session = GetSession())
                {
                    session.Delete(occ);
                    session.Flush();
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException("Permission error");
            }
        }

        public static void deleteAnswer(int answerId)
        {
            var answer = Get.SingleAnswer(answerId);

            using (var session = GetSession())
            {
                session.Delete(answer);
                session.Flush();
            }
        }
    }
}
