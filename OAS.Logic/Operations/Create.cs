﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Operations
{
    using Types;
    using static Session;

    public static class Create
    {
        public static Flag NewFlag(string reason, int questionId, int ownerId)
        {
            using (var session = GetSession())
            {
                var fl = new Flag(ownerId, questionId, reason);

                session.Save(fl);
                session.Flush();

                return fl;
            }
        }

        public static Thread NewThread(IEnumerable<User> users)
        {
            using (var session = GetSession())
            {
                var th = new Thread(users);

                session.Save(th);
                session.Flush();

                return th;
            }
        }

        public static OccupationType NewOccupationType(string title, string description, int creatorId)
        {
            using (var session = GetSession())
            {
                var occ = new OccupationType(title, description, creatorId);
                session.Save(occ);

                return occ;
            }
        }
        public static User NewUser(string username, string password, string firstname, string lastname, string email)
        {
            using (var session = GetSession())
            {
                var user = new User(username, password, firstname, lastname, email);
                session.Save(user);
                return user;
            }
        }

        public static User NewUser(string username, string password, string firstname, string lastname, string email, bool isAdmin = false)
        {
            using (var session = GetSession())
            {
                var user = new User(username, password, firstname, lastname, email, isAdmin);
                session.Save(user);
                return user;
            }
        }

        public static Question NewQuestion(string title, string body, OccupationType occupancy, User Owner)
        {
            using (var session = GetSession())
            {
                var q = new Question(title, body, Owner, occupancy);

                session.Save(q);

                return q;
            }
        }

        public static Occupation NewOccupation(int userId, string company, DateTime start, DateTime? end, OccupationType occupation)
        {
            using (var session = GetSession())
            {
                var o = new Occupation(company, occupation, start, end, Get.SingleUser(userId));

                session.Save(o);

                return o;
            }
        }

        public static Answer NewAnswer(int ownerId, int questionId, string answerBody)
        {
            using (var session = GetSession())
            {
                var a = new Answer(ownerId, questionId, answerBody);

                session.Save(a);

                return a;
            }
        }
    }
}
