﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Objects
{
    using FluentNHibernate.Mapping;
    using Types;
    public class FlagMap : ClassMap<Flag>
    {
        public FlagMap()
        {
            Table("[oas].Flag");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity();

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad().Not.Nullable();

            References(x => x.QuestionFlagged).Column("Question_Id").Not.LazyLoad();
            References(x => x.AnswerFlagged).Column("Answer_Id").Not.LazyLoad();

            Map(x => x.Reason).Not.Nullable();
        }
    }
}
