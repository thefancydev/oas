﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;
    public class MessageMap : ClassMap<Message>
    {
        public MessageMap()
        {
            Table("[oas].Message");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity();

            References(x => x.Sender).Column("Sender_Id").Not.LazyLoad();

            References(x => x.ParentThread).Column("Thread_Id").Not.LazyLoad();

            Map(x => x.Body);

            Map(x => x.SentDate); //TODO Add messaging page and get notifying when message recieved wired up. We're cookin' with gas now!
        }
    }
}
