﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;

    public class ThreadMap : ClassMap<Thread>
    {
        public ThreadMap()
        {
            Table("[oas].Thread");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity();

            HasMany(x => x.Messages).KeyColumn("Thread_Id").Not.LazyLoad();

            HasManyToMany(x => x.Recipients).Table("[oas].ThreadRecipientLookup").ParentKeyColumn("Thread_Id").ChildKeyColumn("Recipient_Id").Not.LazyLoad();
        }
    }
}
