﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;
    public class passwordTokenMap : ClassMap<passwordToken>
    {
        public passwordTokenMap()
        {
            Table("[oas].PasswordToken");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity();

            Map(x => x.Value).Not.Nullable();

            Map(x => x.ExpiryDate).Not.Nullable();

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad();
        }
    }
}
