﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Objects
{
    using FluentNHibernate.Mapping;
    using Types;

    public class LogEntryMap : ClassMap<LogEntry>
    {
        public LogEntryMap()
        {
            Table("[oas].ErrorLog");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.ExceptionMessage);
            Map(x => x.ExceptionType);
            Map(x => x.OccurenceDate);
            Map(x => x.Source);

            References(x => x.LoggedIn).Not.LazyLoad().Column("LoggedIn_Id");
        }
    }
}
