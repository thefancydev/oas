﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Objects
{
    using FluentNHibernate.Mapping;
    using Types;

    public class NotificationMap : ClassMap<Notification>
    {
        public NotificationMap()
        {
            Table("[oas].Notification");
            LazyLoad();

            Id(x => x.Id).GeneratedBy.Identity().Not.Nullable();

            References(x => x.Notified).Column("Notified_Id").Not.LazyLoad();

            Map(x => x.Body).Not.Nullable();

            Map(x => x.Read).Column("[Read]"); //Read is a reserved keyword in SQL...

            Map(x => x.DateSent).Not.Nullable();

            Map(x => x.Link);
        }
    }
}
