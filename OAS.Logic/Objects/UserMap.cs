﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;

    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("[oas].[User]");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.UserName).Not.Nullable();
            Map(x => x.PasswordHash).Not.Nullable();
            Map(x => x.IsAdmin);

            Map(x => x.FirstName).Not.Nullable();
            Map(x => x.LastName).Not.Nullable();
            Map(x => x.Email).Not.Nullable();

            HasMany(x => x.Answers).KeyColumn("Owner_Id").Not.LazyLoad();
            HasMany(x => x.Questions).KeyColumn("Owner_Id").Not.LazyLoad();
            HasMany(x => x.Comments).KeyColumn("Owner_Id").Not.LazyLoad();

            HasMany(x => x.CreatedOccupations).KeyColumn("Creator_Id").Not.LazyLoad(); //? how am I going to cascade this? NOTE: Cascade not needed, deactivate user!

            HasMany(x => x.Occupations).KeyColumn("Owner_Id").Not.LazyLoad();

            Map(x => x.IsActive);

            HasMany(x => x.OwnedFlags).KeyColumn("Owner_Id").Not.LazyLoad();

            HasMany(x => x.NotificationsRecieved).KeyColumn("Notified_Id").Not.LazyLoad();

            HasManyToMany(x => x.SubscribedThreads).Table("[oas].ThreadRecipientLookup").ParentKeyColumn("Recipient_Id").ChildKeyColumn("Thread_Id").Not.LazyLoad();

            HasManyToMany(x => x.SecondedAnswers).Table("[oas].SecondedUsers").ParentKeyColumn("User_Id").ChildKeyColumn("Answer_Id").Not.LazyLoad();
            HasManyToMany(x => x.SecondedQuestions).Table("[oas].SecondedUsers").ParentKeyColumn("User_Id").ChildKeyColumn("Question_Id").Not.LazyLoad();
        }
    }
}
