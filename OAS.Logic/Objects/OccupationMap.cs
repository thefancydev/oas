﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;
    public class OccupationMap : ClassMap<Occupation>
    {
        public OccupationMap()
        {
            Table("[oas].Occupation");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();
            Map(x => x.Company).Not.Nullable();

            References(x => x.BaseOccupation).Column("Base_Id").Not.LazyLoad().Not.Nullable();

            Map(x => x.StartDate).Not.Nullable();
            Map(x => x.EndDate).Nullable(); //see comment on enddate

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad().Not.Nullable();
        }
    }
}
