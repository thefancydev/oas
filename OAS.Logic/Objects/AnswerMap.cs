﻿using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;
    public class AnswerMap : ClassMap<Answer>
    {
        public AnswerMap()
        {
            Table("[oas].Answer");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Body).Not.Nullable();

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad().Not.Nullable();

            HasManyToMany(x => x.SecondedUsers).Table("[oas].[oas].SecondedUsers").ParentKeyColumn("Answer_Id").ChildKeyColumn("User_Id").Not.LazyLoad().Cascade.Delete();

            Map(x => x.Accepted);

            References(x => x.AnsweredQuestion).Column("Question_Id").Not.LazyLoad();
            HasMany(x => x.Comments).KeyColumn("Answer_Id").Not.LazyLoad().Cascade.Delete();

            HasMany(x => x.Flags).KeyColumn("Answer_Id").Not.LazyLoad().Cascade.Delete();
        }
    }
}
