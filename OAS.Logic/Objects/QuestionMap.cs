﻿using FluentNHibernate.Mapping;
using OAS.Logic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAS.Logic.Objects
{
    public class QuestionMap : ClassMap<Question>
    {
        public QuestionMap()
        {
            Table("[oas].Question");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Title).Not.Nullable();
            Map(x => x.Body).Not.Nullable();

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad();
            HasManyToMany(x => x.SecondedUsers).Table("[oas].SecondedUsers").ParentKeyColumn("Question_Id").ChildKeyColumn("User_Id").Not.LazyLoad().Cascade.Delete();

            HasMany(x => x.Answers).KeyColumn("Question_Id").Not.LazyLoad().Cascade.Delete();
            HasMany(x => x.Comments).KeyColumn("Question_Id").Not.LazyLoad().Cascade.Delete();

            References(x => x.AskedOccupation).Column("OccupancyAsked_Id").Not.LazyLoad();

            HasMany(x => x.Flags).KeyColumn("Question_Id").Not.LazyLoad().Cascade.Delete();
        }
    }
}
