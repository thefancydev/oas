﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace OAS.Logic.Objects
{
    using Types;
    public class OccupationTypeMap : ClassMap<OccupationType>
    {
        public OccupationTypeMap()
        {
            Table("[oas].OccupationType");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Title);
            Map(x => x.Description);

            References(x => x.Creator).Column("Creator_Id").Not.LazyLoad().Not.Nullable();
        }
    }
}
