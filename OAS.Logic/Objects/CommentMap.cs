﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
namespace OAS.Logic.Objects
{
    using Types;
    public class CommentMap : ClassMap<Comment>
    {
        public CommentMap()
        {
            Table("[oas].Comment");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Body).Not.Nullable();

            References(x => x.Owner).Column("Owner_Id").Not.LazyLoad();

            References(x => x.CommentedQuestion).Column("Question_Id");
            References(x => x.CommentedAnswer).Column("Answer_Id");
        }
    }
}
