:reset
@echo off
SET /p DeploymentLocation="Deploy to Local or Live: "

for %%* in (../.) do SET product.name=%%~nx*

SET sql.files.directory="scripts"
SET repository.url="git@gitlab.com:roconnor/%product.name%.git"

SET /p DropCreateYesNo="Drop and create the database? (Y/N): "
SET /p TransactionYesNo="Use transaction? (Y/N): "

IF /I %Deploymentlocation% EQU Local (SET database.name="%product.name%.DB" && SET server.name=".\SQLEXPRESS") ELSE (SET database.name="roconnorlivedb" && SET server.name="roconnorlivedb.mssql.somee.com")

IF /I %DropCreateYesNo% EQU Y (IF /I %TransactionYesNo% EQU Y (GOTO DropCreateTran) ELSE (GOTO DropCreate)) ELSE (IF /I %TransactionYesNo% EQU Y (GOTO Tran) ELSE (GOTO Normal))

:DropCreateTran
if /I %DeploymentLocation% EQU Live (rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER=%server.name%;DATABASE=%database.name%;USER ID=oasSQLLogin1;PASSWORD=0@$L1veDB" /drop && rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER=%server.name%;DATABASE=%database.name%;USER ID=roconnor_SQLLogin_1;PASSWORD=ykhfqkd2bm" /t) ELSE (rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /drop && rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /t)

goto:eof

:DropCreate
if /I %DeploymentLocation% EQU Live (rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER="%server.name%";DATABASE="%database.name%";USER ID=oasSQLLogin1;PASSWORD=0@$L1veDB" /drop && rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER=%server.name%;DATABASE=%database.name%;USER ID=roconnor_SQLLogin_1;PASSWORD=ykhfqkd2bm") ELSE (rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /drop && rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url%)

goto:eof

:Tran
if /I %DeploymentLocation% EQU Live (rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER=%server.name%;DATABASE=%database.name%;USER ID=oasSQLLogin1;PASSWORD=0@$L1veDB" /t) ELSE (rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /t)

goto:eof

:Normal
if /I %DeploymentLocation% EQU Live (rh.exe /f=%sql.files.directory% /r=%repository.url% /c="SERVER=%server.name%;DATABASE=%database.name%;USER ID=oasSQLLogin1;PASSWORD=0@$L1veDB") ELSE (rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url%)

goto:eof