CREATE
TABLE [oas].[Config]
([Id] INT IDENTITY(1,1) NOT NULL
,[Owner_Id] INT NOT NULL
,[AllowEmails] BIT NOT NULL
,CONSTRAINT [PK_Config_Id] PRIMARY KEY ([Id])
,CONSTRAINT [FK_Config_Owner] FOREIGN KEY ([Owner_Id]) REFERENCES [oas].[User](Id))
GO