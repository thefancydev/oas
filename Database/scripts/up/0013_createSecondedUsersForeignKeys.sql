ALTER TABLE [oas].[SecondedUsers]
ADD CONSTRAINT [FK_Seconded_User_Id] FOREIGN KEY ([User_Id]) REFERENCES [oas].[User](Id)
GO

ALTER TABLE [oas].[SecondedUsers]
ADD CONSTRAINT [FK_Seconded_Answer_Id] FOREIGN KEY ([Answer_Id]) REFERENCES [oas].[Answer](Id)
GO

ALTER TABLE [oas].[SecondedUsers]
ADD CONSTRAINT [FK_Seconded_Question_Id] FOREIGN KEY ([Question_Id]) REFERENCES [oas].[Question](Id)
GO
