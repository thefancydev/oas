ALTER TABLE [oas].[Question]
ADD CONSTRAINT [FK_Question_Owner_Id] FOREIGN KEY ([Owner_Id]) REFERENCES [oas].[User](Id)
GO
