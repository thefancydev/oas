ALTER TABLE [oas].[ErrorLog]
ADD CONSTRAINT [FK_Error_Owner_Id] FOREIGN KEY ([LoggedIn_Id]) REFERENCES [oas].[User](Id)
GO
