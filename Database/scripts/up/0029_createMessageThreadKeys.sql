ALTER TABLE [oas].[Message]
ADD CONSTRAINT [FK_Message_Sender_Id] FOREIGN KEY ([Sender_Id]) REFERENCES [oas].[User](Id)
GO

ALTER TABLE [oas].[Message]
ADD CONSTRAINT [FK_Message_Thread_Id] FOREIGN KEY ([Thread_Id]) REFERENCES [oas].[Thread](Id)
GO
