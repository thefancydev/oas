CREATE INDEX [TI_Question_Index] ON [oas].[Question]([Id])
GO

CREATE INDEX [TI_Answer_Index] ON [oas].[Answer]([Id])
GO