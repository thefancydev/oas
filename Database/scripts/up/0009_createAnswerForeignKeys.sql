ALTER TABLE [oas].[Answer]
ADD CONSTRAINT [FK_Answer_Owner_Id] FOREIGN KEY ([Owner_Id]) REFERENCES [oas].[User](Id)
GO

ALTER TABLE [oas].[Answer]
ADD CONSTRAINT [FK_Answer_Question_Id] FOREIGN KEY ([Question_Id]) REFERENCES [oas].[Question](Id)
GO
