ALTER TABLE [oas].[Flag]
ADD CONSTRAINT [FK_Flag_Owner_Id] FOREIGN KEY ([Owner_Id]) REFERENCES [oas].[User](Id)
GO

ALTER TABLE [oas].[Flag]
ADD CONSTRAINT [FK_Flag_Question_Id] FOREIGN KEY ([Question_Id]) REFERENCES [oas].[Question](Id)
GO

ALTER TABLE [oas].[Flag]
ADD CONSTRAINT [FK_Flag_Answer_Id] FOREIGN KEY ([Answer_Id]) REFERENCES [oas].[Answer](Id)
GO
