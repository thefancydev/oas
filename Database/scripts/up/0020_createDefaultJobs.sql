INSERT
INTO
[oas].[OccupationType]
([Title]
,[Description]
,[Creator_Id])
VALUES
('Programmer'
,'Software developer'
,1)
GO

INSERT
INTO
[oas].[OccupationType]
([Title]
,[Description]
,[Creator_Id])
VALUES
('Accountant'
,'Monetary Accountant'
,1)
GO

INSERT
INTO
[oas].[OccupationType]
([Title]
,[Description]
,[Creator_Id])
VALUES
('Legal Clerk'
,'Manages legal documents'
,1)
GO

INSERT
INTO
[oas].[OccupationType]
([Title]
,[Description]
,[Creator_Id])
VALUES
('Fireman'
,'Fire fighter'
,1)
GO
