CREATE
TABLE [oas].[Occupation]
([Id] INT IDENTITY(1,1) NOT NULL,
[Base_Id] INT NOT NULL,
[StartDate] DATETIME NOT NULL,
[EndDate] DATETIME NULL,
[CurrentJob] BIT,
[Owner_Id] INT NOT NULL,
CONSTRAINT [PK_Occupation_Id] PRIMARY KEY ([Id]))
GO
