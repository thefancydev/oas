ALTER TABLE [oas].[Question]
ADD CONSTRAINT [FK_Question_Occupancy_Id] FOREIGN KEY ([OccupancyAsked_Id]) REFERENCES [oas].[OccupationType](Id)
GO
