CREATE
TABLE [oas].[OccupationType]
([Id] INT IDENTITY(1,1) NOT NULL,
[Title] NVARCHAR(100) NOT NULL,
[Description] NVARCHAR(MAX) NOT NULL,
[Creator_Id] INT,
CONSTRAINT [PK_Occupation_Type_Id] PRIMARY KEY ([Id]))
GO
