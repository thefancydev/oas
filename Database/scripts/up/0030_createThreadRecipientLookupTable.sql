CREATE
TABLE
[oas].[ThreadRecipientLookup]
([Thread_Id] INT NOT NULL
,[Recipient_Id] INT NOT NULL
,CONSTRAINT [PK_Compound_Thread_Lookup] PRIMARY KEY ([Thread_Id], [Recipient_Id])
,CONSTRAINT [FK_Thread_Lookup_Id] FOREIGN KEY ([Thread_Id]) REFERENCES [oas].[Thread](Id)
,CONSTRAINT [FK_Recipient_Lookup_Id] FOREIGN KEY ([Recipient_Id]) REFERENCES [oas].[User](Id))
GO
