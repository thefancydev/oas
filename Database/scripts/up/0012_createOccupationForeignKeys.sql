ALTER TABLE [oas].[Occupation]
ADD CONSTRAINT [FK_Base_Occupation_Id] FOREIGN KEY ([Base_Id]) REFERENCES [oas].[OccupationType](Id)
GO

ALTER TABLE [oas].[Occupation]
ADD CONSTRAINT [FK_Occupation_Owner_Id] FOREIGN KEY ([Owner_Id]) REFERENCES [oas].[User](Id)
GO
